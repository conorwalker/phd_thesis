_This repository contains code underlying the methodology and results presented in the PhD dissertation:_

**Statistical analysis of short template switch mutations in human genomes** \
by Conor R. Walker
