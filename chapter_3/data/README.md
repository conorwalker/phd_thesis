**significant_template_switch_events.grch38.bed** \
Human genome (GRCh38.p12) coordinates of the mutation clusters associated with each significant event, in BED format.


**significant_template_switch_event_genomic_features.grch38.xlsx** \
BED formatted sheets for each of the genomic features processed. In each genomic feature sheet, I report any intersections between that feature and any significant event-associated mutation cluster coordinates in the GRCh38.p12 genome.


**significant_template_switch_events_pairhmm_output.txt** \
Unidirectional and TSA pairHMM alignments for all significant great ape events.
