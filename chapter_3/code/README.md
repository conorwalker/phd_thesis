## Directory contents


**enrichment** \
identify enrichment of template switch loci within various genomic elements


**motifs** \
identify enriched DNA sequence motifs around ancestral event loci


**physical_properties** \
_dna\_flexibility_: calculate DNA bendability around template switch loci \
_mfe_: calculate GC-regressed DNA MFE secondary structures around template switch loci


**process_msas** \
identify candidate events from multiple alignments of the great apes, filter
these events according to statistical significance and several quality thresholds,
resolve their phylogenetic placement and report summary statistics


**sampling_unidirectional_probabilities** \
randomly sample unidirectional pairHMM log probabilities of alignment blocks
from each pairwise species alignment to generate per-base aligment log-probability distributions


**simulations** \
evolutionary simulations of template switch events to establish a significance
threshold on the log of the ratio between the probability of either a unidirectional
alignment or a template switch alignment
