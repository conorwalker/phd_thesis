#!/usr/local/bin/python

from sys import argv
import re


def determine_point_ordering(swp):
    points = [1,2,3,4]
    return "".join([str(x) for _,x in sorted(zip([int(i) for i in swp],points))])


def main():
    with open(argv[1], "r") as f:
        fi_lines = [line.rstrip() for line in f.readlines()]

    for i, j in enumerate(fi_lines):
        if j.startswith("Template switch process"):
            fi_lines[i] = "Template switch process"

    indices = [i for i, x in enumerate(fi_lines) if x == "Template switch process"]
    id_indices = [i for i, x in enumerate(fi_lines) if x.startswith("Population event")]

    csv_indices = [i for i, x in enumerate(fi_lines) if x.startswith("TS_")]

    microhomology_dic = {}

    key_order = []

    for ind, id_ind, csv_ind in zip(indices, id_indices, csv_indices):

        event_id = fi_lines[id_ind].split(" ")[-1]

        l_to_1, four_to_r, ancC, two_to_three = ind+1, ind+2, ind+4, ind+5
        
        l_to_1 = re.findall(" [AGTC]*[AGTC] ", fi_lines[l_to_1])[0].strip()[::-1]
        four_to_r = re.findall(" [AGTC]*[AGTC] ", fi_lines[four_to_r])[0].strip()
        
        fi_lines[two_to_three] = fi_lines[two_to_three].replace(" \x083", "3")

        two_index = len(fi_lines[two_to_three])-2
        three_index = fi_lines[two_to_three].index(" 3")+3
        
        two_to_three = re.findall(" [AGTC]*[AGTC] ", fi_lines[two_to_three])[0].strip()
       
        try:
            ancC_left = re.findall(" [AGTC]*[AGTC]", fi_lines[ancC][:three_index])[0].strip()[::-1]
        except IndexError:
            ancC_left = ""
        
        try:
            ancC_right = re.findall("[AGTC]*[AGTC]", fi_lines[ancC][two_index:])[0].strip()
        except IndexError:
            ancC_right = ""

        ancC = re.findall(" [AGTC]*[AGTC]", fi_lines[ancC])[0].strip()
    
        one_homology, two_homology = 0, 0

        # check homology between left to point 1 and upstream of point 2

        for l1, aC_r in zip(l_to_1, ancC_right):
            if l1 == aC_r:
                one_homology += 1
            else:
                break

        # check homology between point 4 to right, and downstream of point 3
        for fR, aC_l in zip(four_to_r, ancC_left):
            if fR == aC_l:
                two_homology += 1
            else:
                break

        csv_vals = fi_lines[csv_ind].split(",")
        event_id = csv_vals[0]
        p = [int(i) for i in csv_vals[7:11]]
        event_type = determine_point_ordering(p)

        if event_type in ["3124", "1342"]:
            event_type = "3124, 1342"
        if event_type in ["1432", "3214"]:
            event_type = "1432, 3214"

        key_order.append(event_id)

        microhomology_dic[event_id] = [one_homology, two_homology, len(two_to_three), event_type, event_id]

        #if one_homology == two_homology:
        #    print(event_id, one_homology)
    print("homology_12\thomology_34\tlen_23\tevent_type\tevent_id")
    for k in key_order:
        print("\t".join([str(i) for i in microhomology_dic[k]]))
    

    #with open("{}/summary_analyses/microhomologies.tsv".format(wd), "w") as out_f:
    #    out_f.write("homology_12\thomology_34\tlen_23\n")
    #    sorted_keys = sorted(list(microhomology_dic.keys()))
    #    for k in sorted_keys:
    #        out_f.write("\t".join([str(i) for i in microhomology_dic[k]]) + "\n")


if __name__ == "__main__":
    main()
