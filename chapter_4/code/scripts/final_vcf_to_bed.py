#!/usr/local/bin/python

from sys import argv
import gzip


def main():
    line_dic = {}
    with gzip.open(argv[1], "r") as f:
        for line in f.readlines():
            line = line.decode("utf-8")
            if line.startswith("#"):
                continue
            line_split = line.split("\t")
            cluster_id = line_split[2]
            chrom = line_split[0]
            pos = line_split[1]
            if cluster_id in list(line_dic.keys()):
                line_dic[cluster_id][1].append(pos)
            else:
                line_dic[cluster_id] = [chrom, [pos]]

    for k in sorted(list(line_dic.keys())):
        print(line_dic[k][0] + "\t" + line_dic[k][1][0] + "\t" + line_dic[k][1][-1] + "\t" + str(k))



if __name__ == "__main__":
    main()
