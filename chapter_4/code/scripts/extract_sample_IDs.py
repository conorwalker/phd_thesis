#!/usr/local/bin/python

# import gzip
from sys import argv


def main():
    """
    Take in a vcf with unique cluster IDs assigned.

    Group by cluster ID, check the allele count of all variants in the cluster
    is equal to the number of samples in which the template switch is called.

    Keep only template switches for which the allele count is equal to the number
    of samples with the template switch.
    """

    ids_to_keep = {}

    with open(argv[1], "r") as f:
        for line in f.readlines():
            #line = line.decode("utf-8")
            if line.startswith("#"):
                continue
            line = line.split("\t")
            keep = line[0].split("chr")[1] + ":" + line[1] + ":" + line[3][:10] + ":" + line[4][:10]
            ids_to_keep[keep] = len(line[2].split(";"))
    for k in ids_to_keep.keys():
        print(k)
    pass


if __name__ == "__main__":
    main()
