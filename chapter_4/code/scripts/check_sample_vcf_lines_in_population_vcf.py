#!/usr/local/bin/python

import subprocess
from sys import argv


def main():
    # take in vcf lines associated with the event as input, captured in a
    # bash variable
    in_vcf_lines = argv[1]

    positions = []

    # parse input vcf lines from sample vcf
    for line in in_vcf_lines.split("\n"):
        line = line.split("\t")
        chrom = line[0]
        positions.append(line[1])

    # construct chrom range to query the final population vcf using bcftools
    chrom_range = chrom + ":" + \
            str(int(positions[0])-1) + "-" + \
            str(int(positions[-1])+1)


    # find the event in the final population vcf
    result = subprocess.run(["bcftools", "view",
                             "-r", chrom_range,
                             "final_population_vcfs/reference_ancestral/final_population_template_switches.vcf.gz"],
                            stdout=subprocess.PIPE)

    # decode bcftools view lines and create list
    view_lines = result.stdout.decode("utf-8").split("\n")


    final_vcf_pos = []
    pop_cluster_ids = []

    # find lines corresponding to event from bcftools view
    for record_line in view_lines:
        if record_line.startswith("#"):
            continue
        elif not record_line:
            continue
        else:
            record_line = record_line.split("\t")
            final_vcf_pos.append(int(record_line[1]))
            pop_cluster_ids.append(record_line[2])

    # handle multiallelics
    final_vcf_pos = set(final_vcf_pos)
    positions = set([int(i) for i in list(set(positions))])


    # check all variants associated with the cluster were correctly
    # grouped together. should not be an issue, but doesn't hurt to check.
    assert all(x == pop_cluster_ids[0] for x in pop_cluster_ids)

    # if sample event not in final population vcf (i.e. filtered because it
    # fell in a repeat region), output 0
    # otherwise, output the final population cluster ID
    if positions.issubset(final_vcf_pos):
        print(pop_cluster_ids[0])
    else:
        print(0)


if __name__ == "__main__":
    main()
