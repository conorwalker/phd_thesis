#!/usr/local/bin/python

from sys import argv
import gzip
import subprocess
import os
import pathlib
from collections import Counter

from Bio import SeqIO


def align_seqs(samp_seq, ref_seq):
    """
    Needleman wunsch align two sequences.
    input:
        samp_seq | sample sequence
        ref_seq | reference sequence
    output:
        aligned_sample_seq | NW-aligned sample sequence
        aligned_ref_seq | NW-aligned reference sequence
    """

    seq_align = "tools/seq-align/bin/needleman_wunsch"
    p = subprocess.Popen([seq_align, samp_seq, ref_seq],
                         stdout=subprocess.PIPE)
    align_out = p.communicate()[0]
    aligned_seqs = str(align_out).split("\\n")
    aligned_sample_seq = aligned_seqs[0][2:]
    aligned_ref_seq = aligned_seqs[1]
    return aligned_sample_seq, aligned_ref_seq


def clean_temp_files(temp_name):
    """
    Deletes temporary files used when processing each mutation cluster
    """
    os.remove(temp_name + ".gz")
    os.remove(temp_name + ".gz.tbi")
    os.remove(temp_name + ".ref.fasta")


def main():
    # base directory for analysis pipeline
    base_dir = "1k_genomes/high_coverage/"

    # define the sample and chromosome using system argvs
    sample, chrom = argv[1], argv[2]
    # input cluster vcf file
    vcf_file = base_dir + "sample_cluster_vcfs/{0}/{0}.{1}.vcf.gz".format(sample, chrom)
    temp_dir = "/scratch/vcfs_to_fastas/{}/".format(sample)
    pathlib.Path(temp_dir).mkdir(parents=True, exist_ok=True)

    # temp filenames
    temp_vcf = temp_dir + "{}.{}.vcf".format(sample, chrom)
    bgzip_file = temp_vcf + ".gz"
    ref_fasta_path = temp_vcf + ".ref.fasta"

    # input reference sequence in FASTA format
    ref_path = base_dir + "reference_genome/chr{0}.fa".format(chrom)

    # variables to store values when processing each cluster
    coords = []
    header_lines, var_lines = [], []
    cluster_ids, coords = [], []

    # print header for output .tsv file
    header1 = "sample\tchrom\tcluster_id\tcluster_coords\tcluster_start\t"
    header2 = "cluster_end\taligned_ref_seq\taligned_sample_seq"
    print(header1 + header2)

    # read the cluster vcf file
    with gzip.open(vcf_file, "rb") as vcf_fi:
        for line in vcf_fi.readlines():
            # convert from gzip format
            line = line.decode("utf-8")
            # capture vcf header
            if line.startswith("#"):
                header_lines.append(line)
            else:
                # process cluster lines
                var_lines.append(line)
                line_split = line.split("\t")
                coord = int(line_split[1])
                cluster_id = int(line_split[2])
                cluster_ids.append(cluster_id)
                coords.append(coord)

    # process each unique cluster
    for unique_cluster in set(cluster_ids):
        # get clusters lines
        indices = [i for i, x in enumerate(cluster_ids) if x == unique_cluster]
        cluster_lines = [var_lines[i] for i in indices]
        cluster_coords = [coords[i] for i in indices]

        # skip clusters consisting of variants which are at the same position
        # but with different haplotypes
        # for example, HG00099 chrX cluster_id==138
        if len(cluster_coords) > 1 and \
           any(x > 1 for x in Counter(cluster_coords).values()):
            continue

        # define a string in format chrX:Y-Z for command-line tools
        region_string = "chr" + str(chrom) + ":" + \
                        str(cluster_coords[0] - 300) + "-" + \
                        str(cluster_coords[-1] + 300)

        # check nothing went wrong in grouping positions into clusters
        assert abs(cluster_coords[0] - cluster_coords[-1]) < 1000

        # lines which will be used to generate a temporary vcf file
        out_lines = header_lines + cluster_lines

        # create a temporary vcf file in either /tmp/ or /scratch/
        #with tempfile.NamedTemporaryFile(mode="w",
        #                                 dir=temp_dir,
        #                                 suffix=".vcf") as temp_file:
        with open(temp_vcf, "w") as temp_file:
            # write temporary vcf
            for line in out_lines:
                temp_file.write(line)

            # reset temporary vcf reading to start of file
            temp_file.seek(0)

            # generate bgzipped temp vcf and tabix index it
            with open(bgzip_file, "w") as bgzip_out:
                subprocess.call(["bgzip", "-c", temp_file.name],
                                stdout=bgzip_out)
            subprocess.call(["tabix", "-f", bgzip_file])

            # generate temporary reference FASTA, FASTA file required for
            # use with bcftools consensus
            with open(ref_fasta_path, "w") as ref_fasta_file:
                subprocess.call(["samtools", "faidx", ref_path, region_string],
                                stdout=ref_fasta_file)

            # generate temporary sample sequence, no need to store in FASTA
            # so the output is captured directly to reduce the number of
            # generated temporary files
            samp_fasta = subprocess.check_output(["bcftools", "consensus",
                                           "-H",
                                           "A",
                                           "-s", sample,
                                           "-f", ref_fasta_path,
                                           bgzip_file],
                                           encoding="utf8")

            # remove header and new line characters from output
            samp_seq = "".join(samp_fasta.strip().split("\n")[1:])

            # read the sequence from the generated reference FASTA file
            ref_seq = str(SeqIO.read(ref_fasta_path, "fasta").seq)

            # align temporary sequences
            aligned_samp_seq, aligned_ref_seq = align_seqs(samp_seq, ref_seq)

            # define .tsv output lines
            out_contents = [
                sample,
                "chr{}".format(chrom),
                unique_cluster,
                ",".join([str(cc) for cc in cluster_coords]),
                cluster_coords[0],
                cluster_coords[-1],
                aligned_ref_seq,
                aligned_samp_seq
            ]

            # print output lines
            print("\t".join([str(oc) for oc in out_contents]))


if __name__ == "__main__":
    main()
