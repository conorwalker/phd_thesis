#!/usr/local/bin/python

from sys import argv


def main():
    line_dic = {}

    with open(argv[1], "r") as in_fi:
        for line in in_fi.readlines():
            if line.startswith("#"):
                continue
            line_split = line.split("\t")
            
            line_chrom = line_split[0]
            line_pos = line_split[1]
            line_id = int(line_split[2])
        
            if line_id not in line_dic.keys():
                line_dic[line_id] = [line_chrom, line_pos]
            else:
                line_dic[line_id].append(line_pos)
    
    for k, v in line_dic.items():
        bed_entry = v[0] + "\t" + v[1] + "\t" + v[-1]
        print(bed_entry)



if __name__ == "__main__":
    main()
