#!/usr/local/bin/python

from sys import argv


def main():
    seen = []
    count = 0
    with open(argv[1], "r") as f:
        for line in f.readlines():
            line = line.split("\t")
            curr_id = line[2]
            if curr_id not in seen:
                count += 1
                seen.append(curr_id)
            line[2] = "TS_{}".format(count)
            line = "\t".join(line)
            print(line.rstrip())
    pass


if __name__ == "__main__":
    main()
