#!/usr/local/bin/python

# import os
import subprocess
import random
import shutil
import pathlib
from sys import argv
from Bio import SeqIO
#import numpy as np


def align_seqs(samp_seq, ref_seq):
    """
    Needleman wunsch align two sequences.
    input:
        samp_seq | sample sequence
        ref_seq | reference sequence
    output:
        aligned_sample_seq | NW-aligned sample sequence
        aligned_ref_seq | NW-aligned reference sequence
    """

    seq_align = "tools/seq-align/bin/needleman_wunsch"
    p = subprocess.Popen([seq_align, samp_seq, ref_seq],
                         stdout=subprocess.PIPE)
    align_out = p.communicate()[0]
    aligned_seqs = str(align_out).split("\\n")
    aligned_sample_seq = aligned_seqs[0][2:]
    aligned_ref_seq = aligned_seqs[1]
    return aligned_sample_seq, aligned_ref_seq


def generate_random_coords(sample, n_to_sample, sample_seed):
    base_dir = "1k_genomes/high_coverage/"
    chrom_lens = base_dir + "misc_files/GRCh38.p13.chrom_lengths.noY.tsv"
    gap_file = base_dir + "misc_files/hg38_gaps.bed"
    bed_random = subprocess.Popen(["bedtools", "random",
                                   "-l", "99", "-n", str(n_to_sample*5),
                                   "-g", chrom_lens,
                                   "-seed", str(sample_seed)],
                                  stdout=subprocess.PIPE)

    bed_gap_inter = subprocess.Popen(["bedtools", "intersect",
                                      "-v", "-a", "stdin", "-b", gap_file],
                                     stdin=bed_random.stdout,
                                    stdout=subprocess.PIPE)

    bed_sort_out = [i.decode("utf-8").strip() for i in bed_gap_inter.stdout]
    bed_sort_write = bed_sort_out[:int(n_to_sample)]
    random.shuffle(bed_sort_write)

    return bed_sort_write


def convertToNumber(s):
    return int.from_bytes(s.encode(), 'little')


def main():
    base_dir = "1k_genomes/high_coverage/"
    tsa = base_dir + "tools/tsa_pairhmm/tsa_pairhmm"

    baseline_score = "-0.0789914\t100\tno_overlap"

    sample = argv[1]

    sample_seed = convertToNumber(sample)

    random.seed(sample_seed)

    n_to_sample = 2000

    sorted_coords = generate_random_coords(sample, n_to_sample, sample_seed)

    temp_dir = "/scratch/sampling_logprobs/{}/".format(sample)
    pathlib.Path(temp_dir).mkdir(parents=True, exist_ok=True)

    seq_out_dir = base_dir + "sampling_log_probabilities/sampled_sequences/{}".format(sample)
    pathlib.Path(seq_out_dir).mkdir(parents=True, exist_ok=True)

    logprob_out_dir = base_dir + "sampling_log_probabilities/sampled_logprobs"
    pathlib.Path(logprob_out_dir).mkdir(parents=True, exist_ok=True)
    
    logprob_fi = logprob_out_dir + "/{}.txt".format(sample)

    temp_vcf = temp_dir + "{}.vcf".format(sample)
    bgzip_file = temp_vcf + ".gz"
    ref_fasta_path = temp_vcf + ".ref.fasta"

    # count and process extra in case of errors in bcftools consensus
    total_sampled = 0

    for i_coord, coord in enumerate(sorted_coords):
        if total_sampled == n_to_sample:
            break
        coord = coord.split("\t")
        chrom, start, end = coord[:3]
        region_string = "{}:{}-{}".format(chrom, start, end)

        try:
            extract_coords = subprocess.Popen(
                ["bcftools", "view", "-c1", "-U", "-I",
                 "-s", sample,
                 "-r", region_string,
                 base_dir + "ftp_vcfs/{}.vcf.gz".format(chrom)],
                stdout=subprocess.PIPE
            )
            sampled_coords = subprocess.check_output(["bcftools", "norm", "-m-any"],
                                                     stdin=extract_coords.stdout).decode("utf-8")
            extract_coords.wait()
            # if only header is output, no variants found
            print("len:", len(sampled_coords.split("\n")))
            print(sampled_coords.split("\n")[-5:])
            if sampled_coords.split("\n")[-2].startswith("#"):
                # append "perfect alignment" score
                # get this using "get_baseline_100nt_logprob.sh baseline_100nts.fa"
                
                with open(logprob_fi, "a+") as out_logprob_file:
                    out_logprob_file.write(baseline_score + "\n")
            else:
                ref_path = base_dir + "reference_genome/{0}.fa".format(chrom)
                out_fasta = seq_out_dir + "/{}.fa".format(i_coord)
                # call bcftools consensus
                with open(temp_vcf, "w") as temp_file:
                    for line in sampled_coords:
                        temp_file.write(line)

                    temp_file.seek(0)

                    with open(bgzip_file, "w") as bgzip_out:
                        subprocess.call(["bgzip", "-c", temp_file.name],
                                        stdout=bgzip_out)

                    subprocess.call(["tabix", "-f", bgzip_file])

                    # generate temporary reference FASTA, FASTA file required for
                    # use with bcftools consensus
                    with open(ref_fasta_path, "w") as ref_fasta_file:
                        subprocess.call(["samtools", "faidx", ref_path, region_string],
                                        stdout=ref_fasta_file)
                    
                    # generate temporary sample sequence, no need to store in FASTA
                    # so the output is captured directly to reduce the number of
                    # generated temporary files
                    samp_fasta = subprocess.check_output(["bcftools", "consensus",
                                                   "-H",
                                                   "A",
                                                   "-s", sample,
                                                   "-f", ref_fasta_path,
                                                   bgzip_file],
                                                   encoding="utf8")
                    
                    samp_seq = "".join(samp_fasta.strip().split("\n")[1:])
                    ref_seq = str(SeqIO.read(ref_fasta_path, "fasta").seq)
                    aligned_samp_seq, aligned_ref_seq = align_seqs(samp_seq, ref_seq)
                    
                with open(out_fasta, "w") as out_file:
                    out_file.write(">GRCh38.{0}.{1}\n{3}\n>{2}.{0}.{1}\n{4}\n".format(chrom, i_coord, sample, aligned_samp_seq, aligned_ref_seq))

                with open(logprob_fi, "a+") as out_logprob_file:
                    subprocess.call([tsa, "--pair", out_fasta,
                                     "--divergence", "0.001",
                                     "--rho", "0.04",
                                     "--lambda", "3",
                                     "--ts_n_switches", "200",
                                     "--ts_n_clusters", "148032",
                                     "--ts_23_fragment_length", "10",
                                     "--sample_unidirectional"], stdout=out_logprob_file)
        
        except subprocess.CalledProcessError:
            continue
        
        total_sampled += 1
    shutil.rmtree(temp_dir)
    pass


if __name__ == "__main__":
    main()
