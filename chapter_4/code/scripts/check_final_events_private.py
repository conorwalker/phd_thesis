#!/usr/local/bin/python

from sys import argv
import gzip
import subprocess


def main():
    line_dic = {}

    line_content_dic = {}

    with gzip.open(argv[1], "r") as in_vcf:
        for line in in_vcf.readlines():
            line = line.decode("utf-8")
            if line.startswith("#"):
                # print(line)
                continue
            else:
                line_split = line.split("\t")
                
                line_chrom = line_split[0]
                line_pos = line_split[1]
                line_id = int(line_split[2])

                if line_id not in line_dic.keys():
                    line_dic[line_id] = [line_chrom, line_pos]
                    line_content_dic[line_id] = [line]
                else:
                    line_dic[line_id].append(line_pos)
                    line_content_dic[line_id].append(line)
   

    merged_sample_dic = {}

    with open(argv[2], "r") as in_merged_samples:
        for line in in_merged_samples:
            if line.startswith("#"):
                continue
            line_split = line.split("\t")
            line_id = line_split[0] + ":" + line_split[1]
            line_samples = line_split[2]
            clean_line_samples = []
            for sample in line_samples.split(";"):
                sample = sample.split("_")[0]
                clean_line_samples.append(sample)
            merged_sample_dic[line_id] = clean_line_samples


    events_to_keep = []

    private, not_private = 0, 0

    for k, v in line_dic.items():
        samples_with_vars = []
        for val in v[1:]:
            var_id = v[0] + ":" + val
            samples_with_vars.append(merged_sample_dic[var_id])
        all_same = all(x == samples_with_vars[0] for x in samples_with_vars)
        if all_same:
            events_to_keep.append(v[0] + "\t" + str(int(v[1])-1) + "\t" + str(int(v[-1])+1))
            event_region = v[0] + ":" + v[1] + "-" + v[-1]
            curr_samples = ",".join(samples_with_vars[0])
            result = subprocess.run(["bcftools", "view",
                                     "-r", event_region,
                                     "-s", "^{}".format(curr_samples),
                                     argv[1]], stdout=subprocess.PIPE)
            view_lines = result.stdout.decode("utf-8").split("\n")
            private_bools = []
            for record_line in view_lines:
                if record_line.startswith("#"):
                    continue
                elif not record_line:
                    continue
                else:
                    rec_split = record_line.split("\t")
                    gts = rec_split[rec_split.index("GT")+1:]
                    new_same = all(x == "0|0" for x in gts)
                    if new_same:
                        private_bools.append(1)
                    else:
                        private_bools.append(0)

            if all(x == 1 for x in private_bools):
                private += 1
            else:
                not_private += 1

        print("{}/{} ({}%) private".format(private, not_private+private, round(private/(not_private+private), 2)*100))
    pass


if __name__ == "__main__":
    main()
