wd=1k_genomes/high_coverage

final_pop_fi=final_population_vcfs/reference_ancestral/final_population_template_switches.vcf.gz

final_vcf_path="$wd"/"$final_pop_fi"

rm "$wd"/single_all_events_printed.txt

for pop_event in $(zcat "$final_vcf_path" | grep -v "#" | cut -f3 | uniq | sort -V); do
    echo "==================================================================================" >> "$wd"/single_all_events_printed.txt
    echo "" >> "$wd"/single_all_events_printed.txt
    sed -n -e  "/Population event $pop_event\$/{p; :loop n; p; /=/q; b loop}" "$wd"/all_events_printed.txt >> "$wd"/single_all_events_printed.txt
    echo "" >> "$wd"/single_all_events_printed.txt
    echo "" >> "$wd"/single_all_events_printed.txt
    echo "" >> "$wd"/single_all_events_printed.txt
done
