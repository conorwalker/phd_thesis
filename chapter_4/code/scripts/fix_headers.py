#!/usr/local/bin/python

import gzip
import subprocess
import os
from sys import argv


def main():
    out_lines = []

    sample = argv[1].split("/")[-1].split(".")[0]

    with gzip.open(argv[1], "rb") as f:
        for line in f.readlines():
            line = line.decode("utf-8")
            if line.startswith("#"):
                if line.startswith("#CHROM"):
                    line = line.split("\t")
                    if line[-1] == sample + "\n":
                        out_lines.append("\t".join(line))
                        continue
                    out_lines.append("\t".join(line[:-1]) + "\n")
                    continue
                out_lines.append(line)
            else:
                line = line.split("\t")
                if line[2].split("_")[0] != sample:
                    line[2] = sample + "_" + line[2]
                line = "\t".join(line)
                out_lines.append(line)

    os.remove(argv[1])

    with open(argv[1] + ".tmp", "w") as f:
        for line in out_lines:
            f.write(line)

    with open(argv[1], "w") as bgzip_vcf:
        subprocess.call(["bgzip", "-c", argv[1] + ".tmp"], stdout=bgzip_vcf)

    subprocess.call(["tabix", "-p", "vcf", argv[1]])
    
    os.remove(argv[1] + ".tmp")


if __name__ == "__main__":
    main()
