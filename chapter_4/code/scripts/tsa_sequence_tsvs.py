#!/usr/local/bin/python

import subprocess
import gzip
from sys import argv


def keep_max_LPR(csv_lines):
    """
    Given a list of csv TSA output lines, retain the line with the highest
    LPR value (the value in the final field).
    """
    LPR_list = []
    for line in csv_lines:
        LPR_list.append(float(line.split(",")[-1]))
    return csv_lines[LPR_list.index(max(LPR_list))]


def main():
    work_dir = "1k_genomes/high_coverage/"
    tsa = work_dir + "tools/tsa_pairhmm/tsa_pairhmm"
    sample = argv[1]
    chrom = argv[2]
    in_tsv = work_dir + "sample_cluster_sequences/{0}/{0}.{1}.tsv.gz".format(sample, chrom)
    
    sample_path = work_dir + "TSA_sample_ancestral/{0}/{0}.{1}.csv".format(sample, chrom)
    ref_path = work_dir + "TSA_reference_ancestral/{0}/{0}.{1}.csv".format(sample, chrom)
    
    ref_anc_out_lines = []
    samp_anc_out_lines = []

    with gzip.open(in_tsv, "r") as f:
        next(f)
        for line in f.readlines():
            line = [li.strip() for li in line.decode("utf-8").split("\t")]
            cluster_id = line[2]
            ref_seq, sample_seq = line[-2], line[-1]

            tsa_anc_ref = subprocess.check_output([tsa, "--scan",
                                                   "--divergence", "0.001",
                                                   "--rho", "0.04",
                                                   "--lambda", "3",
                                                   "--ts_n_switches", "200",
                                                   "--ts_n_clusters", "148032",
                                                   "--ts_23_fragment_length", "10",
                                                   "--anc_seq", ref_seq,
                                                   "--desc_seq", sample_seq,
                                                   "--ID", cluster_id])

            tsa_anc_sample = subprocess.check_output([tsa, "--scan",
                                                      "--divergence", "0.001",
                                                      "--rho", "0.04",
                                                      "--lambda", "3",
                                                      "--ts_n_switches", "200",
                                                      "--ts_n_clusters", "148032",
                                                      "--ts_23_fragment_length", "10",
                                                      "--desc_seq", ref_seq,
                                                      "--anc_seq", sample_seq,
                                                      "--ID", cluster_id])

            # if no cluster found after NW alignment, don't process
            if len(tsa_anc_ref.decode("utf-8").split("\n")) == 3:
                continue

            anc_ref_out = tsa_anc_ref.decode("utf-8").split("\n")[1:-2]
            anc_sample_out = tsa_anc_sample.decode("utf-8").split("\n")[1:-2]

            if len(anc_ref_out) == 1:
                tsa_anc_ref = anc_ref_out[0]
            else:
                tsa_anc_ref = keep_max_LPR(anc_ref_out)

            if len(anc_sample_out) == 1:
                tsa_anc_sample = anc_sample_out[0]
            else:
                tsa_anc_sample = keep_max_LPR(anc_sample_out)
            
            ref_anc_out_lines.append(tsa_anc_ref)
            samp_anc_out_lines.append(tsa_anc_sample)

    with open(ref_path, "w") as f:
        for line in ref_anc_out_lines:
            f.write(line + "\n")

    with open(sample_path, "w") as f:
        for line in samp_anc_out_lines:
            f.write(line + "\n")


if __name__ == "__main__":
    main()
