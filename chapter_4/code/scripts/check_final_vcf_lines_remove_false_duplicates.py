#!/usr/local/bin/python

import gzip
from sys import argv


def main():
    # argv[1] is the merged samples file
    # argv[2] is the vcf extracted from the ftp_vcfs directory
    # containing duplicate positions
    merged_vcf = argv[1]
    extracted_vcf = argv[2]

    merge_pos = []

    with open(merged_vcf, "r") as f:
        for line in f.readlines():
            if line.startswith("#"):
                continue
            line = line.split("\t")
            for alt_i in range(len(line[4].split(","))):
                alt_var = line[4].split(",")[alt_i]
                merge_pos.append("_".join([line[0], line[1], line[3], alt_var]))

    with gzip.open(extracted_vcf, "rb") as f:
        for line in f.readlines():
            line = line.decode("utf-8")
            if line.startswith("#"):
                print(line.strip())
                continue
            line = line.split("\t")
            line_id = "_".join([line[0], line[1], line[3], line[4]])
            if line_id in merge_pos:
                print("\t".join(line).strip())
                


if __name__ == "__main__":
    main()
