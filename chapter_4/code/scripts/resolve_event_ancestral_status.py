#!/usr/local/bin/python

import gzip
import pathlib
import requests
import subprocess
import os
from sys import argv


def fetch_alignment(chrom_range):
    # print(chrom_range)
    server = "https://rest.ensembl.org"
    ext = "/alignment/region/homo_sapiens/{}?species_set=homo_sapiens;species_set=pan_troglodytes;method=LASTZ_NET".format(chrom_range)
    r = requests.get(server+ext, headers={"Content-Type": "application/json"})
    try:
        decoded = r.json()[0]["alignments"]
        human_seq = [i for i in decoded if "homo_sapiens" in i.values()][0]["seq"]
        chimp_seq = [i for i in decoded if "pan_troglodytes" in i.values()][0]["seq"]
        return human_seq, chimp_seq
    except (KeyError, IndexError):
        return "", ""


def unidirectional_align(wd, human_seq, chimp_seq):
    # needleman wunsch used as the TSA pairHMM expects aligned sequences as input
    # this can be fixed in the tsa_pairhmm.cpp code but using NW for now as
    # adds next to no overhead
    tsa = wd + "tools/tsa_pairhmm/tsa_pairhmm"
    seq_align = wd + "tools/seq-align/bin/needleman_wunsch"
    p = subprocess.Popen([seq_align, human_seq, chimp_seq],
                         stdout=subprocess.PIPE)
    align_out = p.communicate()[0]
    aligned_seqs = str(align_out).split("\\n")
    aligned_human_seq = aligned_seqs[0][2:]
    aligned_chimp_seq = aligned_seqs[1]

    align = subprocess.check_output([tsa,
                                     "--anc_seq", aligned_chimp_seq,
                                     "--desc_seq", aligned_human_seq,
                                     "--sample_unidirectional"])
    return float(align.decode("utf-8").split("\t")[0])




def main():
    wd = "1k_genomes/high_coverage/"

    sample = argv[1]

    sample_vcf = wd + "filtered_vcfs/merged/{0}/{0}.vcf.gz".format(sample)

    variants = []

    flank_size = 20

    with gzip.open(sample_vcf, "rb") as in_fi:
        for line in in_fi.readlines():
            line = line.decode("utf-8").strip()
            if line.startswith("#"):
                continue
            else:
                variants.append(line.split("\t"))

    unique_clusters = [i[0] + "_" + i[2] for i in variants]
    
    cluster_lines = [[value for value in variants if key == value[0] + "_" + value[2]] for key in set(unique_clusters)]
    
    chrom_clusters = {}

    chroms = [str(i+1) for i in list(range(22))] + ["X"]

    for c in chroms:
        chrom_clusters[c] = {}

    for cluster in cluster_lines:
        chrom, cluster_id = cluster[0][0].split("chr")[1], cluster[0][2]
        chrom_clusters[chrom][cluster_id] = {}


    for c in chroms:
        seq_fi = wd + "sample_cluster_sequences/{0}/{0}.{1}.tsv.gz".format(sample, c)
        with gzip.open(seq_fi, "rb") as seq_tsv:
            for line in seq_tsv.readlines():
                line = line.decode("utf-8").strip().split("\t")
                if line[0].startswith("#"):
                    continue
                cluster_id = line[2]
                if cluster_id in chrom_clusters[c].keys():
                    chrom_clusters[c][cluster_id]["start"] = line[4]
                    chrom_clusters[c][cluster_id]["end"] = line[5]
                    chrom_clusters[c][cluster_id]["bed"] = "chr{}:{}-{}".format(c, int(line[4])-flank_size, int(line[5])+flank_size)
                    chrom_clusters[c][cluster_id]["ref"] = line[6].replace("-", "")
                    chrom_clusters[c][cluster_id]["samp"] = line[7].replace("-", "")

    # 300 nts around the cluster were extracted in the tsvs
    # to get the start of this sequence for testing homology, use the 300nt
    # position - the desired flank size as the start index
    index_start = 300 - flank_size
    resolve_status = ""

    for chrom, chrom_cluster_dic in chrom_clusters.items():
        # chrom_test = chrom
        for cluster_id, cluster_dic in chrom_cluster_dic.items():
            human_seq, chimp_seq = fetch_alignment(cluster_dic["bed"])
            chrom_clusters[chrom][cluster_id]["chimp"] = chimp_seq
            chrom_clusters[chrom][cluster_id]["lastz_human"] = human_seq
            human_seq_nogaps = human_seq.replace("-","")
            chimp_seq_nogaps = chimp_seq.replace("-","")

            compare_ref_seq = chrom_clusters[chrom][cluster_id]["ref"][index_start:index_start+len(human_seq_nogaps)]
            compare_samp_seq = chrom_clusters[chrom][cluster_id]["samp"][index_start:index_start+len(human_seq_nogaps)]
            try:
                assert human_seq_nogaps == compare_ref_seq
                assert human_seq_nogaps != ""
                assert len(human_seq) >= flank_size * 2
                ref_align = unidirectional_align(wd, compare_ref_seq, chimp_seq_nogaps)
                samp_align = unidirectional_align(wd, compare_samp_seq, chimp_seq_nogaps)
                
                if ref_align > samp_align:
                    resolve_status = "reference_ancestral"
                elif samp_align > ref_align:
                    resolve_status = "sample_ancestral"
                else:
                    # e.g. if the alignments are equally probable
                    resolve_status = "unresolved"
            except AssertionError:
                # this will happen if an appropriate alignment block is not
                # present in the LASTZ alignment of human and chimp
                # on the ENSEMBL servers
                resolve_status = "unresolved"

            chrom_clusters[chrom][cluster_id]["resolve_status"] = resolve_status
    
    resolve_line = '##INFO=<ID=Ancestral_Status,Number=.,Type=String,Description="Indicates if the sample or reference is ancestral, or if this is unresolved.">'
    
    header_lines = []
    out_lines = {"sample_ancestral": [],
                 "reference_ancestral": [],
                 "unresolved": []}

    with gzip.open(sample_vcf, "rb") as in_fi:
        for line in in_fi.readlines():
            line = line.decode("utf-8").strip()
            if line.startswith("#"):
                header_lines.append(line)
            else:
                line_split = line.split("\t")
                chrom, cluster_id = line_split[0].split("chr")[1], line_split[2]
                ref_anc_gt, samp_anc_gt = line_split[-2], line_split[-1]
                resolve_status = chrom_clusters[chrom][cluster_id]["resolve_status"]
                
                if resolve_status == "unresolved":
                    if samp_anc_gt.startswith("./."):
                        resolve_status = "reference_ancestral"
                    elif ref_anc_gt.startswith("./."):
                        resolve_status = "sample_ancestral"

                line_split[-4] += ";Ancestral_Status={}".format(resolve_status)

                # if the ancestral human sequence had no variant call, label
                # as unresolved
                # the existence of these cases were not checked for,
                # but should be excluded regardless
                if resolve_status == "sample_ancestral" and samp_anc_gt.startswith("./."):
                    resolve_status = "reference_ancestral"
                
                if resolve_status == "reference_ancestral" and ref_anc_gt.startswith("./."):
                    resolve_status = "sample_ancestral"


                if resolve_status == "sample_ancestral":
                    del line_split[-2]
                if resolve_status == "reference_ancestral":
                    del line_split[-1]
                line_split[2] = sample + "_" + line_split[2]
                line = "\t".join(line_split)
                out_lines[resolve_status].append(line)

    # get last ##INFO= occurrence in the VCF header, append the
    # vcf header containing resolve_status after this occurrence
    ins_i = [i for i, e in enumerate(header_lines) if e.startswith("##INFO")][-1] + 1
    header_lines.insert(ins_i, resolve_line)
    
    header_lines[-1] = "\t".join(header_lines[-1].split()[:-1])

    sample_base_dir = wd + "final_sample_vcfs/"

    for status, file_lines in out_lines.items():
        out_dir = sample_base_dir + "{}/{}/".format(status, sample)
        pathlib.Path(out_dir).mkdir(parents=True, exist_ok=True)
        out_path = out_dir + "{}.vcf".format(sample)
        with open(out_path, "w") as out_vcf:
            for line in header_lines:
                out_vcf.write(line + "\n")
            for line in file_lines:
                out_vcf.write(line + "\n")
  
        if status != "unresolved":
            with open(out_path + ".tmp", "w") as outfi:
                subprocess.call(["bedtools", "intersect", "-header",
                                 "-a", out_path,
                                 "-b", "filtered_vcfs/{0}/{1}/{1}.vcf.gz".format(status, sample),
                                 "-u"],
                                 stdout=outfi)
            os.replace(out_path + ".tmp", out_path)

        with open(out_path + ".gz", "w") as bgzip_vcf:
            subprocess.call(["bgzip", "-c", out_path], stdout=bgzip_vcf)
        
        subprocess.call(["tabix", "-p", "vcf", out_path+".gz"])
        os.remove(out_path)


if __name__ == "__main__":
    main()
