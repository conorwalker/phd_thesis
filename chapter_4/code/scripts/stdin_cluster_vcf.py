#!/usr/bin/env python3

import fileinput
import numpy as np


def all_equal(iterator):
    iterator = iter(iterator)
    try:
        first = next(iterator)
    except StopIteration:
        return True
    return all(first == rest for rest in iterator)


def update_vcf_lines(cluster_i, split_lines):
    joined_lines = []
    for line in split_lines:
        line[2] = str(cluster_i)
        joined_lines.append("\t".join(line))
    for stdout_line in joined_lines:
        print(stdout_line.strip())


def evaluate_cluster(cluster_split_lines):
    try:
        gt_pos = cluster_split_lines[0].index("GT")
    except ValueError:
        gt_pos = [n for n, l in enumerate(cluster_split_lines[0]) if l.startswith("GT")][0]
    haplotypes = []
    for line in cluster_split_lines:
        haplotype_list = line[gt_pos+1:]
        # process haplotypes on chrX which come with stat annotation
        if len(haplotype_list[0].split(":")) > 1:
            haplotype_list = [gt.split(":")[0] for gt in haplotype_list]
        haplotype_list[-1] = haplotype_list[-1].strip()
        haplotypes.append(haplotype_list)
    haplotypes = np.asarray(haplotypes)
    haplotypes = haplotypes[:, (haplotypes == haplotypes[0, :]).all(0)]
    if haplotypes.shape[1] == 0:
        return False
    else:
        return True


def process_single_lines(cluster_lines, cluster_count):
    # out_lines = []
    for single_line in cluster_lines:
        len_ref, len_alt = len(single_line[3]), len(single_line[4])
        # evaluate single snps
        if len_ref == 1 and len_alt == 1:
            continue
        # evaluate single indel
        # keep insertions >= 4nt in length, discard lone deletions
        if abs(len_alt - len_ref) >= 5:  # single insertion >= 5nt
            cluster_count += 1
            update_vcf_lines(cluster_count, [single_line])
    return cluster_count


def main():
    cluster_count = 0

    cluster_dic = {}

    for line in fileinput.input():
        # line = line.decode("utf-8")
        if line.startswith("#"):
            #header_lines.append(line)
            print(line.strip())
        else:
            line_split = line.split("\t")
            current_pos = int(line_split[1])
            # define new cluster
            if not cluster_dic:
                cluster_dic["coords"] = [current_pos]
                cluster_dic["lines"] = [line_split]
                continue

            pos_diff = current_pos - cluster_dic["coords"][-1]

            if pos_diff <= 10:
                #print("pd:", current_pos)
                cluster_dic["coords"].append(current_pos)
                cluster_dic["lines"].append(line_split)
                continue

            # if no cluster found, process current line and line-1
            # if "cluster" of identical coordinates found, process
            # each alternative allele separately
            # this step captures candidate single templated insertions
            if len(cluster_dic["coords"]) == 1:
                # append current line to cluster list for processing
                cluster_dic["lines"].append(line_split)
                cluster_count = process_single_lines(cluster_dic["lines"], cluster_count)
                cluster_dic.clear()

            # evaluate clusters
            else:
                is_cluster = evaluate_cluster(cluster_dic["lines"])
                #is_cluster = 1
                if is_cluster:
                    cluster_count += 1
                    update_vcf_lines(cluster_count, cluster_dic["lines"])
                else:
                    cluster_dic["lines"].append(line_split)
                    cluster_count = process_single_lines(cluster_dic["lines"], cluster_count)
                cluster_dic.clear()


if __name__ == "__main__":
    main()
