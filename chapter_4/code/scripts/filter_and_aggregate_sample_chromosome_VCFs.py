#!/usr/local/bin/python

import gzip
import subprocess
#import pathlib
import os
from sys import argv


def main():
    wd = "1k_genomes/high_coverage/"
    # retrieve one argument containing the sample name
    sample = argv[1]
        
    cluster_dic = {}
    chromosomes = [str(i+1) for i in list(range(22))] + ["X"]


    for refsamp in ["reference", "sample"]:
        cluster_dic[refsamp] = {}
        in_fi = wd + "TSA_{}_ancestral_filtered/{}.csv".format(refsamp, sample)
        for chrom in chromosomes:
            cluster_dic[refsamp][chrom] = []
        with open(in_fi, "r") as f:
            tsa_lines = [i.strip() for i in f.readlines()]
        for line in tsa_lines:
            line = line.split(",")
            cluster_id, chrom = line[0], line[-1]
            cluster_dic[refsamp][chrom].append(cluster_id)

    sample_dir = wd + "sample_cluster_vcfs/{}/".format(sample)

    header_lines = []
    ref_lines = {"reference": [], "sample": []}

    for chrom in chromosomes:
        print("chrom:", chrom)
        chrom_fi = sample_dir + "{}.{}.vcf.gz".format(sample, chrom)
        with gzip.open(chrom_fi, "rb") as vcf_fi:
            for line in vcf_fi.readlines():
                line = line.decode("utf-8")
                # record header lines from first file, continue otherwise
                if line.startswith("#"):
                    if chrom == "X":
                        header_lines.append(line)
                    continue
                # process each vcf line
                for refsamp in list(ref_lines.keys()):
                    #print(line.split("\t")[1])
                    if line.split("\t")[2] in cluster_dic[refsamp][chrom]:
                        # print(line)
                        ref_lines[refsamp].append(line)

    #for k in list(ref_lines.keys()) + ["both"]:
    #    out_dir = "filtered_vcfs/{0}_ancestral/{1}/".format(k, sample)
    #    pathlib.Path(out_dir).mkdir(parents=True, exist_ok=True)

    for k, cluster_lines in ref_lines.items():
        out_path = "filtered_vcfs/{0}_ancestral/{1}/{1}.vcf".format(k, sample)
        with open(out_path, "w") as out_vcf:
            for line in header_lines + cluster_lines:
                out_vcf.write(line)
        with open(out_path + ".gz", "w") as bgzip_vcf:
            subprocess.call(["bgzip", "-c", out_path], stdout=bgzip_vcf)
        subprocess.call(["tabix", "-p", "vcf", out_path+".gz"])
        os.remove(out_path)

    ref = "filtered_vcfs/reference_ancestral/{0}/{0}.vcf.gz".format(sample)
    samp = "filtered_vcfs/sample_ancestral/{0}/{0}.vcf.gz".format(sample)
 
    out_path = "filtered_vcfs/merged/{0}/{0}.vcf.gz".format(sample)
    with open(out_path, "w") as merged_fi:
        subprocess.call(["bcftools", "merge", "-Oz", ref, samp, "--force-samples"],
                        stdout=merged_fi)
    
    subprocess.call(["tabix", "-p", "vcf", out_path])



if __name__ == "__main__":
    main()
