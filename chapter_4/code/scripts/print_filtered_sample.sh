sample=$1;
ancseq=$2;

filter_fi=TSA_"$ancseq"_ancestral_filtered/"$sample".csv

seq_dir=sample_cluster_sequences/"$sample"

vcf_dir=sample_cluster_vcfs/"$sample"

tsa="tools/tsa_pairhmm/tsa_pairhmm"

counter=0

while read line; do
    #echo $line
    cluster_id=$(echo $line | awk -F, '{print $1}');
    chrom=$(echo $line | awk -F, '{print $NF}');
    #echo "chrom: $chrom"
    seq_line=$(zcat "$seq_dir"/"$sample"."$chrom".tsv.gz | awk -v cluster_id="$cluster_id" '($3==cluster_id)');
    if [[ "$ancseq" == "sample" ]]; then
        anc=$(echo "$seq_line" | cut -f 8);
        desc=$(echo "$seq_line" | cut -f 7);
    elif [[ "$ancseq" == "reference" ]]; then
        anc=$(echo "$seq_line" | cut -f 7);
        desc=$(echo "$seq_line" | cut -f 8);
    fi

    vcf_lines=$(zcat "$vcf_dir"/"$sample"."$chrom".vcf.gz | awk -v cluster_id="$cluster_id" '($3==cluster_id)' | cut -f1-5);
    in_final=$(python scripts/check_sample_vcf_lines_in_population_vcf.py "$vcf_lines");

    if [ "$in_final" != "0" ]; then
        counter=$((counter+1))

        single_vcf_line=$(echo $vcf_lines | head -n 3);
        echo "=================================================================================="
        echo "---------"
        echo "Event $counter"
        echo "Population event $in_final"
        echo "---------"
        echo
        echo "VCF entries"
        echo "-----------"
        echo "$vcf_lines"
        echo
        echo "BED entry"
        echo "---------"
        bed_entry=$(echo $single_vcf_line | awk -v count=$counter -v pop_id=$in_final '{print $1"\t"$2"\t"$(NF-3)"\t"count"\t"pop_id}');
        echo "$bed_entry"
        echo
        echo
        echo "TSA pairHMM output"
        echo "------------------"
        echo "$in_final,$line"
        $tsa --divergence 0.001 \
             --rho 0.04 \
             --lambda 3 \
             --ts_n_switches 200 \
             --ts_n_clusters 148032 \
             --ts_23_fragment_length 10 \
             --ID "$chrom" \
             --print-file-string "$line" --anc_seq "$anc" --desc_seq "$desc"
        echo "=================================================================================="
        echo
        echo
        echo
    fi
done < "$filter_fi"
