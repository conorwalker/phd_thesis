#!/usr/bin/env python3

import fileinput
import numpy as np


def all_equal(iterator):
    iterator = iter(iterator)
    try:
        first = next(iterator)
    except StopIteration:
        return True
    return all(first == rest for rest in iterator)


def update_vcf_lines(cluster_i, split_lines):
    joined_lines = []
    for line in split_lines:
        line[2] = str(cluster_i)
        joined_lines.append("\t".join(line))
    for stdout_line in joined_lines:
        print(stdout_line.strip())


def evaluate_cluster(cluster_split_lines):
    try:
        gt_pos = cluster_split_lines[0].index("GT")
    except ValueError:
        gt_pos = [n for n, l in enumerate(cluster_split_lines[0]) if l.startswith("GT")][0]
    haplotypes = []
    for line in cluster_split_lines:
        haplotype_list = line[gt_pos+1:]
        # process haplotypes on chrX which come with stat annotation
        if len(haplotype_list[0].split(":")) > 1:
            haplotype_list = [gt.split(":")[0] for gt in haplotype_list]
        haplotype_list[-1] = haplotype_list[-1].strip()
        haplotypes.append(haplotype_list)
    haplotypes = np.asarray(haplotypes)
    haplotypes = haplotypes[:, (haplotypes == haplotypes[0, :]).all(0)]
    if haplotypes.shape[1] == 0:
        return False
    else:
        return True


def process_single_lines(cluster_lines, cluster_count, prev_diff):
    for single_line in cluster_lines:
        if prev_diff <= 10:
            prev_diff = 1000
        else:
            cluster_count += 1
        update_vcf_lines(cluster_count, [single_line])
    return cluster_count, prev_diff


def main():
    cluster_count = 0
    cluster_dic = {}
    previous_pos = 0
    current_pos = 0
    prev_diff = 1000
    last_processed_was_single = 0
    count_before = 0

    for line in fileinput.input():
        if line.startswith("#"):
            print(line.strip())
        else:
            line_split = line.split("\t")
            previous_pos = current_pos
            current_pos = int(line_split[1])
            current_chrom = line_split[0]
            
            # define new cluster
            if not cluster_dic:
                if last_processed_was_single == 1:
                    prev_diff = current_pos - previous_pos
                    last_processed_was_single = 0
                cluster_dic["chrom"] = current_chrom
                cluster_dic["coords"] = [current_pos]
                cluster_dic["lines"] = [line_split]
                continue
            
            pos_diff = current_pos - cluster_dic["coords"][-1]

            if pos_diff <= 10 and current_chrom == cluster_dic["chrom"]:
                cluster_dic["coords"].append(current_pos)
                cluster_dic["lines"].append(line_split)
                continue

            # if no cluster found, process current line and line-1
            # if "cluster" of identical coordinates found, process
            # each alternative allele separately
            # this step captures candidate single templated insertions
            if len(cluster_dic["coords"]) == 1:
                # append current line to cluster list for processing
                cluster_dic["lines"].append(line_split)
                count_before = cluster_count
                
                cluster_count, prev_diff = process_single_lines(cluster_dic["lines"], cluster_count, prev_diff)
                if cluster_count != count_before:
                    last_processed_was_single = 1
                cluster_dic.clear()

            # evaluate clusters
            else:
                is_cluster = True
                if is_cluster:
                    if prev_diff <= 10:
                        prev_diff = 1000
                    else:
                        cluster_count += 1
                    update_vcf_lines(cluster_count, cluster_dic["lines"])
                else:
                    cluster_dic["lines"].append(line_split)
                    cluster_count, prev_diff = process_single_lines(cluster_dic["lines"], cluster_count)
                cluster_dic.clear()
                cluster_dic["chrom"] = current_chrom
                cluster_dic["coords"] = [current_pos]
                cluster_dic["lines"] = [line_split]


if __name__ == "__main__":
    main()
