**Snakefile** \
A snakemake implementation of my template switch event discovery pipeline.


**scripts/** \
Contains a set of scripts which are called by Snakemake to facilitate event discovery and output processing.


**notebooks/** \
Contains a set of Jupyter notebooks used to perform a variety of analyses and generate figures throughout this chapter.
