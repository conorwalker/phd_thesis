**final_population_template_switches.csv.gz** \
Contains pairHMM csv output for all significant human population template switch events.

**final_population_template_switches.txt.gz** \
Contains printed alignments for all significant human population template switch events.

**final_population_template_switches.bed** \
Contains GRCh38-indexed coordinates for all significant human population template switch events.

**final_population_template_switches_annotated.vcf.gz** \
A bgzipped VCF containing all significant human population template switch events.

**Ice-Trios_denovo_events.txt** \
Contains printed alignments of the two significant template switches identified in Icelandic family trios.

**strong_linkage_events_plink_GWAScatalog.tsv** \
For the 49 template switch variants in strong linkage with a GWAS catalog variant, the plink and GWAS catalog output are provided in tsv format.

**perfect_linkage_events_plink_GWAScatalog.tsv** \
For the 2 template switch variants in perfect linkage with a GWAS catalog variant, the plink and GWAS catalog output are provided in tsv format.
