fi=$1;
chrom=$2;
out_folder=$3;

bedtools intersect -a $1 -b genome_features/encode/nucleosome_occupancy/chr"$chrom"_ENCFF000VNN_grch37.bed -wo >> "$out_folder"/chr"$chrom"_nucleosome_range_signal.bed
