fi=$1;
chrom=$2;
while read line; do
    echo $line | awk '{print $1"\t"$2"\t"$3"\t"$4}' > /tmp/"$chrom"_tmpfi.bed;
    bedtools closest -a /tmp/"$chrom"_tmpfi.bed -b genome_features/encode/nucleosome_occupancy/chr"$chrom"_ENCFF000VNN_grch37.bed -D a -k 1000 | awk '{print $1"\t"$6"\t"$7"\t"$8"\t"$2"_"$9}' >> /tmp/chr"$chrom"_nucleosome_signal_tmp.bed;
    bedtools sort -i /tmp/chr"$chrom"_nucleosome_signal_tmp.bed >> chr"$chrom"_nucleosome_signal.bed;
    rm /tmp/chr"$chrom"_nucleosome_signal_tmp.bed; 
    rm /tmp/"$chrom"_tmpfi.bed;
done < $fi;
