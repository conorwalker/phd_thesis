**scripts/** \
Contains scripts used to assess nucleosome occupancy. Many scripts used in this chapter are included in the subdirectories associated with previous chapters.


**notebooks/** \
Contains a set of Jupyter notebooks used to perform a variety of analyses and generate figures throughout this chapter.
